import uuidv4 from 'uuid/v4'

let todos = []

// return the todos array
const getTodos = () => todos

// Check for existing saved data
const loadTodos = () => {
    const todosJSON = localStorage.getItem('todos')

    try {
        return todosJSON ? JSON.parse(todosJSON) : []
    } catch (e) {
        return []
    }   
}

// save todos to localStorage
const saveTodos = () => {
    localStorage.setItem('todos', JSON.stringify(todos))
}

// create a new todo
const createTodo = (text) => {
        todos.push({
            id: uuidv4(),
            text,
            completed: false
        })
    
    saveTodos()
}

// remove a todo
const removeTodo = (id) => {
    const todoIndex = todos.findIndex((todo) => todo.id === id)

    if (todoIndex > -1) {
        todos.splice(todoIndex, 1)
        saveTodos()
    }
}

// toggle the completed value for a given todo
const toggleTodo = (id) => {
    const todo = todos.find((todo) => todo.id === id)

    if (todo) {
        todo.completed = !todo.completed
        saveTodos()
    }
}

todos = loadTodos()

export { getTodos, loadTodos, createTodo, removeTodo, toggleTodo }